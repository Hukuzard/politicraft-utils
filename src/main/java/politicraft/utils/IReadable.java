package politicraft.utils;

@FunctionalInterface
public interface IReadable {
    String readable();
}
