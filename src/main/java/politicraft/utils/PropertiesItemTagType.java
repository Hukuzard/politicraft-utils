package politicraft.utils;

import com.google.gson.Gson;
import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataType;

public class PropertiesItemTagType implements PersistentDataType<String, ItemProperties> {

    @Override
    public Class<String> getPrimitiveType() {
        return String.class;
    }

    @Override
    public Class<ItemProperties> getComplexType() {
        return ItemProperties.class;
    }

    @Override
    public String toPrimitive(ItemProperties complex, PersistentDataAdapterContext context) {
        return new Gson().toJson(complex);
    }

    @Override
    public ItemProperties fromPrimitive(String primitive, PersistentDataAdapterContext context) {
        return new Gson().fromJson(primitive, ItemProperties.class);
    }
}
