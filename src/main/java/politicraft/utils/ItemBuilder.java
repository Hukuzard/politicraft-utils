package politicraft.utils;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ItemBuilder {
    private ItemStack base;

    private ItemBuilder(Material material) {
        this(new ItemStack(material));
    }

    private ItemBuilder(ItemStack itemStack) {
        this.base = new ItemStack(itemStack);
    }

    public static ItemBuilder of(Material material) {
        return new ItemBuilder(material).unbreakable().hideAttributes();
    }

    public static ItemBuilder of(ItemStack itemStack) {
        return new ItemBuilder(itemStack).unbreakable().hideAttributes();
    }

    public static ItemBuilder empty() {
        return of(Material.BARRIER);
    }

    public ItemBuilder amount(int amount) {
        this.base.setAmount(Math.max(amount, 1));
        return this;
    }

    public ItemBuilder type(Material material) {
        this.base.setType(material);
        return this;
    }

    public ItemBuilder unbreakable() {
        return this.breakAttr(true);
    }

    public ItemBuilder breakable() {
        return this.breakAttr(false);
    }

    public ItemBuilder hideAttributes() {
        return this.meta(meta -> meta.addItemFlags(ItemFlag.values()));
    }

    public ItemBuilder display(String name) {
        return this.meta(meta -> meta.setDisplayName(name));
    }

    public ItemBuilder lores(List<String> lores, boolean clear) {
        return this.meta(meta -> {
            List<String> currentLores = Optional.ofNullable(meta.getLore()).orElse(new LinkedList<>());

            if (clear) {
                currentLores.clear();
            }

            currentLores.addAll(lores.stream().filter(Objects::nonNull).collect(Collectors.toList()));

            meta.setLore(currentLores);
        });
    }

    public ItemBuilder lores(List<String> lores) {
        return this.lores(lores, false);
    }

    public ItemBuilder lore(String lore) {
        return this.lores(Collections.singletonList(lore), false);
    }

    public <T, Z> ItemBuilder tag(Plugin plugin, String nameSpace, PersistentDataType<T, Z> itemTagType, Z value) {
        return this.meta(meta -> meta.getPersistentDataContainer().set(new NamespacedKey(plugin, nameSpace), itemTagType, value));
    }

    public <T, Z> ItemBuilder tag(Plugin plugin, IsPersistentItem<T, Z> itemTag, Z value) {
        return this.tag(plugin, itemTag.namespace(), itemTag.persistentData(), value);
    }

    public ItemStack build() {
        return this.base;
    }

    public ItemStack[] build(int amount) {
        return IntStream.range(0, Math.max(1, amount)).boxed().map(i -> this.base.clone()).toArray(ItemStack[]::new);
    }

    private ItemBuilder meta(Consumer<ItemMeta> apply) {
        Optional.ofNullable(this.base.getItemMeta()).ifPresent(meta -> {
            apply.accept(meta);
            this.base.setItemMeta(meta);
        });

        return this;
    }

    private ItemBuilder breakAttr(boolean breakable) {
        return this.meta(meta -> meta.setUnbreakable(breakable));
    }
}
