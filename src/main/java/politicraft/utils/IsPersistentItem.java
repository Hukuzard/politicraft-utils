package politicraft.utils;

import org.bukkit.persistence.PersistentDataType;

public interface IsPersistentItem<T, Z> {
    String namespace();

    PersistentDataType<T, Z> persistentData();
}
