package politicraft.utils;

import org.bukkit.persistence.PersistentDataType;

import java.util.function.Supplier;

public class PersistentItem<T, Z> implements IsPersistentItem<T, Z> {
    private final String nameSpace;
    private final Supplier<PersistentDataType<T, Z>> supplier;

    public PersistentItem(Supplier<PersistentDataType<T, Z>> supplier, String nameSpace) {
        this.supplier = supplier;
        this.nameSpace = nameSpace;
    }

    @Override
    public String namespace() {
        return this.nameSpace;
    }

    @Override
    public PersistentDataType<T, Z> persistentData() {
        return this.supplier.get();
    }
}

