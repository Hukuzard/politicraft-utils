package politicraft.utils;

import org.bukkit.ChatColor;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class TextBuilder implements IReadable {
    private final Predicate<ChatColor> isColor = ChatColor::isColor;
    private final Predicate<ChatColor> isDark = chatColor -> chatColor.name().startsWith("DARK");
    private final List<ChatColor> lights = this.filter(this.isColor.and(this.isDark.negate()));
    private final List<ChatColor> darks = this.filter(this.isColor.and(this.isDark));
    /**
     * Le text de base à modifier
     */
    private List<String> basesText;
    private String separator = " ";

    /**
     * Constructeur privé
     *
     * @param baseText Le texte de base à donner au builder. Il peut être transformé de base
     */
    private TextBuilder(String baseText) {
        this.basesText = new LinkedList<>();

        if (baseText != null && !baseText.isEmpty()) {
            this.basesText.add(baseText);
        }
    }

    /**
     * Instanciation basique
     *
     * @param text Le texte de base
     * @return Une nouvelle instance du builder avec pour base le texte passé en paramètre
     */
    public static TextBuilder of(String text) {
        return new TextBuilder(text);
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder of(IReadable text) {
        return of(text.readable());
    }

    /**
     * Instanciation d'une version sombre du texte
     */
    public static TextBuilder dark(String text) {
        return of(text).darkColors();
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder dark(IReadable text) {
        return dark(text.readable());
    }

    /**
     * Instanciation d'un texte en rouge
     *
     * @see TextBuilder#danger(String)
     */
    public static TextBuilder danger(String text) {
        return of(text).danger();
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder danger(IReadable text) {
        return danger(text.readable());
    }

    /**
     * Instanciation d'un texte en vert
     *
     * @see TextBuilder#success(String)
     */
    public static TextBuilder success(String text) {
        return of(text).success();
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder success(IReadable text) {
        return success(text.readable());
    }

    /**
     * Instanciation d'un texte en bleu
     *
     * @see TextBuilder#info(String)
     */
    public static TextBuilder info(String text) {
        return of(text).info();
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder info(IReadable text) {
        return info(text.readable());
    }

    /**
     * Instanciation d'un texte en jaune
     *
     * @see TextBuilder#warning(String)
     */
    public static TextBuilder warning(String text) {
        return of(text).warning();
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder warning(IReadable text) {
        return warning(text.readable());
    }

    /**
     * Instanciation d'un texte souligné
     *
     * @see TextBuilder#underline(String)
     */
    public static TextBuilder underline(String text) {
        return of(text).underline();
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder underline(IReadable text) {
        return underline(text.readable());
    }

    /**
     * Instanciation d'un texte en gras
     *
     * @see TextBuilder#bold(String)
     */
    public static TextBuilder bold(String text) {
        return of(text).bold();
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder bold(IReadable text) {
        return bold(text.readable());
    }

    /**
     * Instanciation d'un texte en italic
     *
     * @see TextBuilder#italic(String)
     */
    public static TextBuilder italic(String text) {
        return of(text).italic();
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder italic(IReadable text) {
        return italic(text.readable());
    }

    /**
     * Instanciation d'un texte barré
     *
     * @see TextBuilder#strike(String)
     */
    public static TextBuilder strike(String text) {
        return of(text).strike();
    }

    /**
     * Alternative avec une instance de {@link IReadable}
     */
    public static TextBuilder strike(IReadable text) {
        return strike(text.readable());
    }

    /**
     * Composition de plusieurs intances. Les effets seront appliqués linéairements
     */
    public static TextBuilder compose(TextBuilder... others) {
        return of(Stream.of(others).filter(Objects::nonNull).reduce(of(""), TextBuilder::and));
    }

    /**
     * Jointure de plusieurs instances par un caractère donné
     */
    public static TextBuilder join(String by, List<IReadable> values) {
        return of(values.stream().map(IReadable::readable).collect(Collectors.joining(TextBuilder.of(by).reset().readable())));
    }

    /**
     * Jointure de plusieurs instances par un caractère donné
     * <p>
     * Override de la méthode {@link #join(String, List)} en passant un nombre illimité de paramètres
     */
    public static TextBuilder join(String by, IReadable... values) {
        return join(by, Arrays.asList(values));
    }

    public static ChatColor getChatColor(String value) {
        try {
            return ChatColor.valueOf(value.toUpperCase());
        } catch (Exception e) {
            return Stream
                    .of(ChatColor.values())
                    .filter(chatColor -> chatColor.name().equalsIgnoreCase(value) || chatColor.toString().equals(value))
                    .findAny()
                    .orElse(ChatColor.RESET);
        }
    }

    //----

    @Override
    public String readable() {
        return String.join(String.format("%s%s", ChatColor.RESET, this.separator), this.basesText);
    }

    /**
     * Composition unitaire de l'instance courante avec une autre instance
     */
    public TextBuilder and(TextBuilder other) {
        if (other != null) {
            TextBuilder newInstance = of("");
            newInstance.basesText.addAll(Stream.concat(this.basesText.stream(), other.basesText.stream()).collect(Collectors.toList()));
            return newInstance;
        }

        return this;
    }

    public TextBuilder and(IReadable other) {
        return and(TextBuilder.of(Optional.ofNullable(other).map(IReadable::readable).orElse("")));
    }

    /**
     * Application d'une couleur au texte
     */
    public TextBuilder color(ChatColor colorOfFormat) {
        this.transform(s -> {
            if (colorOfFormat.isFormat()) {
                String colors = ChatColor.getLastColors(s);
                return String.format("%s%s%s", colors, colorOfFormat, s.replace(colors, ""));
            }

            return String.format("%s%s", colorOfFormat, s);
        });

        return this;
    }

    /**
     * Changement du texte en rouge
     */
    public TextBuilder danger() {
        return this.color(ChatColor.RED);
    }

    /**
     * Changement du texte en vert
     */
    public TextBuilder success() {
        return this.color(ChatColor.GREEN);
    }

    /**
     * Changement du texte en bleu
     */
    public TextBuilder info() {
        return this.color(ChatColor.BLUE);
    }

    /**
     * Changement du texte en jaune
     */
    public TextBuilder warning() {
        return this.color(ChatColor.GOLD);
    }

    /**
     * Application d'un effet souligné
     */
    public TextBuilder underline() {
        return this.color(ChatColor.UNDERLINE);
    }

    /**
     * Application d'un effet gras
     */
    public TextBuilder bold() {
        return this.color(ChatColor.BOLD);
    }

    /**
     * Application d'un effet italique
     */
    public TextBuilder italic() {
        return this.color(ChatColor.ITALIC);
    }

    /**
     * Application d'un effet barré
     */
    public TextBuilder strike() {
        return this.color(ChatColor.STRIKETHROUGH);
    }

    /**
     * Application d'une transformation de texte
     */
    public TextBuilder transform(Function<String, String> apply) {
        if (apply != null) {
            IntStream.range(0, this.basesText.size()).boxed().forEach(i -> this.basesText.set(i, apply.apply(this.basesText.get(i))));
        }

        return this;
    }

    /**
     * Transformation en lowercase
     */
    public TextBuilder lowercase() {
        return this.transform(String::toLowerCase);
    }

    /**
     * Transformation en UPPERCASE
     */
    public TextBuilder uppercase() {
        return this.transform(String::toUpperCase);
    }

    /**
     * Stip du texte
     */
    public TextBuilder strip() {
        return this.transform(ChatColor::stripColor);
    }

    /**
     * R.à.z des effets
     */
    public TextBuilder reset() {
        return this.transform(s -> String.format("%s%s", ChatColor.RESET, s));
    }

    public TextBuilder thenReset() {
        return this.transform(s -> String.format("%s%s", s, ChatColor.RESET));
    }

    /**
     * Changement de toutes les couleurs dans leur correspondance claire, si elle existe
     */
    public TextBuilder lightColors() {
        return this.replace(this.darks, chatColor -> chatColor.name().replace("DARK_", ""));
    }

    /**
     * Changement de toutes les couleurs dans leur correspondance sombre, si elle existe
     */
    public TextBuilder darkColors() {
        return this.replace(this.lights, chatColor -> String.format("DARK_%s", chatColor.name()));
    }

    /**
     * Configuration du caractère séparateur pour restituer la chaîne de caractères finale
     */
    public TextBuilder separator(String separator) {
        this.separator = separator == null || separator.isEmpty() ? "" : separator;
        return this;
    }

    /**
     * Versions similaires avec des instances de {@link IReadable}
     */

    //---
    private TextBuilder replace(List<ChatColor> colors, Function<ChatColor, String> mapper) {
        colors.forEach(color -> {
            ChatColor replaced = getChatColor(mapper.apply(color));

            if (replaced != ChatColor.RESET) {
                this.transform(s -> s.replace(color.toString(), replaced.toString()));
            }
        });

        return this;
    }

    private List<ChatColor> filter(Predicate<ChatColor> predicate) {
        return Stream.of(ChatColor.values()).filter(predicate).collect(Collectors.toList());
    }
}
