package politicraft.utils;

import java.util.LinkedList;
import java.util.List;

public class ItemProperties {
    private final List<Pair<String, String>> properties;

    public ItemProperties() {
        this(new LinkedList<>());
    }

    public ItemProperties(List<Pair<String, String>> properties) {
        this.properties = properties;
    }

    public ItemProperties addProperty(String key, String value) {
        if (key != null && value != null) {
            this.properties.add(new Pair<>(key, value));
        }

        return this;
    }

    public String getProperty(String key, String defaultValue) {
        return this.properties
                .stream()
                .filter(pair -> pair.getKey().equals(key))
                .findFirst()
                .map(Pair::getValue)
                .orElse(defaultValue);
    }
}
